Programming Exercise
=====================================

The purpose of the exercise is to write a mechanism that will eliminate duplicate entries.  The objective
is to prevent loading a file if its content has not changed.  The intent is for your implementation to be
called each time a file is to be written to a caching service.  Updating a file causes the cached copy to
be updated, which is slow and causes clients to wait.  So this updating must be done only when a file has
actually changed; otherwise the post should be ignored.

We are asking you to implement only the duplicate detection.  Use the provided `Deduplicator` interface to
do this.  A test case suite is provided to check your work.

The basic requirements are:

* The first time your implementation sees a particular path, it should assume the data is not a duplicate.
* Each subsequent time the same path is seen, the data is a duplicate if the content is the same (or has a very high probability of being the same) as *the previous data* that was seen for that same path.
* Your implementation cannot place any constraints on the data contents.
* (optional) Your solution should detect a duplicate that appears just after the application is restarted.

When completing this exercise, give it the same seriousness of purpose that you would for a production-ready
application that you would have to support.  We believe that a suitable candidate should be able to
complete this exercise in a single undistracted evening.  The optional requirement may add significant
development time, so if you do not have time to implement it, that will not be held against you.

To submit, provide a link to your own repository (either a `git clone` command or a link to a website that
provides that information).  You do not have to host it on bitbucket.org, but it must be publicly
accessible so that we can obtain it.

We will evaluate your submission according to these criteria:

* Correctness of your solution for the stated requirements.
* Your design/implementation choices.
* What, if anything, you have changed in provided files.  For example, you are welcome to change the pom.xml if you need to add dependencies.
* Coding style and comments.  We are not asking you to follow any particular style guide, but we do expect clean, well-written code that is easy to follow.
* Development habits that can be gleaned from reviewing the history of your repo.

Thank you for spending your valuable time to complete this exercise and for considering employment at
Turner Broadcasting.  Good luck.
